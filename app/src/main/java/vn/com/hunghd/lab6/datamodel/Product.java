package vn.com.hunghd.lab6.datamodel;

public class Product {

    private int id;
    private String name;
    private String displayName;
    private double price;
    private int categoryId;

    public Product() {
    }

    public Product(int id, String name, String displayName, double price, int categoryId) {
        this.id = id;
        this.name = name;
        this.displayName = displayName;
        this.price = price;
        this.categoryId = categoryId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", displayName='" + displayName + '\'' +
                ", price=" + price +
                ", categoryId=" + categoryId +
                '}';
    }
}
