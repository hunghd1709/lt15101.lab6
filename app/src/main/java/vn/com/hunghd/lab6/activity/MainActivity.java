package vn.com.hunghd.lab6.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import vn.com.hunghd.lab6.R;
import vn.com.hunghd.lab6.adapter.CategoryAdapter;
import vn.com.hunghd.lab6.dao.CategoryDAO;
import vn.com.hunghd.lab6.datamodel.Category;
import vn.com.hunghd.lab6.util.DbHelper;

public class MainActivity extends AppCompatActivity {

    public static String TAG = "lt15101.lab7";
    private ListView lvCategory;
    private CategoryAdapter categoryAdapter;
    private List<Category> categories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //
        mapComponents();
        loadData();
        registerForContextMenu(lvCategory);
    }

    private void mapComponents(){
        lvCategory = findViewById(R.id.lvCategory);
    }

    private void loadData(){
        DbHelper dbHelper = new DbHelper(MainActivity.this);
        CategoryDAO categoryDAO = new CategoryDAO(dbHelper.getWritableDatabase());
        categories = categoryDAO.readAll();
        categoryAdapter = new CategoryAdapter(this, R.layout.row_category, categories);
        lvCategory.setAdapter(categoryAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.category_option_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_category:
                createCategory();
                return true;
            case R.id.help:

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.category_context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.update_category:
                updateCategory(categories.get(info.position));
                return true;
            case R.id.delete_category:
                DbHelper dbHelper = new DbHelper(MainActivity.this);
                CategoryDAO categoryDAO = new CategoryDAO(dbHelper.getWritableDatabase());
                categoryDAO.delete(categories.get(info.position).getId());
                categories.remove(info.position);
                categoryAdapter.notifyDataSetChanged();
                return true;
            default:
                return super.onContextItemSelected(item);
        }

    }

    public void createCategory() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        // Get the layout inflater
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setTitle("Create new category")
                .setView(inflater.inflate(R.layout.category_create, null))
                // Add action buttons
                .setPositiveButton("Create", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        AlertDialog alertDialog  = (AlertDialog) dialog;
                        EditText etCategoryName = alertDialog.findViewById(R.id.etCategoryName);
                        EditText etCategoryDisplayName = alertDialog.findViewById(R.id.etCategoryDisplayName);
                        Category category = new Category(etCategoryName.getText().toString(),
                                etCategoryDisplayName.getText().toString());

                        DbHelper dbHelper = new DbHelper(MainActivity.this);
                        CategoryDAO categoryDAO = new CategoryDAO(dbHelper.getWritableDatabase());
                        long newId;
                        if((newId = categoryDAO.create(category))>0) {
                            categories.add(new Category((int)newId, etCategoryName.getText().toString(),
                                    etCategoryDisplayName.getText().toString()));
                            categoryAdapter.notifyDataSetChanged();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //
                    }
                });
        builder.show();

    }

    public void updateCategory(final Category category) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        // Get the layout inflater
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.category_create, null);
        EditText etCategoryName = view.findViewById(R.id.etCategoryName);
        EditText etCategoryDisplayName = view.findViewById(R.id.etCategoryDisplayName);
        etCategoryName.setText(category.getName());
        etCategoryDisplayName.setText(category.getDisplayName());

        builder.setTitle("Update category")
                .setView(view)
                // Add action buttons
                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        AlertDialog alertDialog  = (AlertDialog) dialog;
                        EditText etCategoryName = alertDialog.findViewById(R.id.etCategoryName);
                        EditText etCategoryDisplayName = alertDialog.findViewById(R.id.etCategoryDisplayName);
                        category.setDisplayName(etCategoryDisplayName.getText().toString());
                        category.setName(etCategoryName.getText().toString());

                        DbHelper dbHelper = new DbHelper(MainActivity.this);
                        CategoryDAO categoryDAO = new CategoryDAO(dbHelper.getWritableDatabase());
                        long newId;
                        if((newId = categoryDAO.update(category))>0) {
                            categoryAdapter.notifyDataSetChanged();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //
                    }
                });

        builder.show();

    }
}
