package vn.com.hunghd.lab6.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import vn.com.hunghd.lab6.datamodel.Category;
import vn.com.hunghd.lab6.datamodel.Product;

public class CategoryDAO {

    private SQLiteDatabase db;

    public static final String TABLE_NAME = "categories";
    public static final String COLUMN_NAME_ID = "id";
    public static final String COLUMN_NAME_NAME = "name";
    public static final String COLUMN_NAME_DISPLAY_NAME = "display_name";

    public static final String CREATE_TABLE_CATEGORIES = "CREATE TABLE " + TABLE_NAME +"("
            + COLUMN_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_NAME_NAME + " VARCHAR NOT NULL,"
            + COLUMN_NAME_DISPLAY_NAME + " VARCHAR NOT NULL"
            + ");";

    public static final String DROP_TABLE_CATEGORIES = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public CategoryDAO() {
    }

    public CategoryDAO(SQLiteDatabase db) {
        this.db = db;
    }

    public SQLiteDatabase getDb() {
        return db;
    }

    public void setDb(SQLiteDatabase db) {
        this.db = db;
    }

    public long create(Category category){
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_NAME, category.getName());
        values.put(COLUMN_NAME_DISPLAY_NAME, category.getDisplayName());
        return db.insert(TABLE_NAME, null, values);
    }

    public Category read(int id){
        String[] columns = {COLUMN_NAME_ID, COLUMN_NAME_NAME, COLUMN_NAME_DISPLAY_NAME};
        String selection = COLUMN_NAME_ID + " = ?";
        String[] selectionArgs = {id+""};
        Cursor cursor = db.query(TABLE_NAME, columns, selection, selectionArgs, null,null, null );

        if(cursor.moveToNext()){
            return new Category(cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_ID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_NAME_NAME)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_NAME_DISPLAY_NAME)));
        }

        return null;
    }

    public List readAll(){
        List categories = new ArrayList();
        String[] columns = {COLUMN_NAME_ID, COLUMN_NAME_NAME, COLUMN_NAME_DISPLAY_NAME};

        Cursor cursor = db.query(TABLE_NAME, columns, null, null, null,null, null );

        while(cursor.moveToNext()){
            categories.add(new Category(cursor.getInt(cursor.getColumnIndex(COLUMN_NAME_ID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_NAME_NAME)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_NAME_DISPLAY_NAME))));
        }

        return categories;
    }

    public int update(Category category){
        ContentValues values = new ContentValues();
        String whereClause = COLUMN_NAME_ID + " = ?";
        String[] whereArgs = {category.getId()+""};

        values.put(COLUMN_NAME_NAME, category.getName());
        values.put(COLUMN_NAME_DISPLAY_NAME, category.getDisplayName());

        return db.update(TABLE_NAME, values, whereClause, whereArgs);
    }

    public int delete(int id){
        String whereClause = COLUMN_NAME_ID + " = ?";
        String[] whereArgs = {id+""};

        return db.delete(TABLE_NAME, whereClause, whereArgs);
    }
}
