package vn.com.hunghd.lab6.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import vn.com.hunghd.lab6.R;
import vn.com.hunghd.lab6.datamodel.Category;

public class CategoryAdapter extends ArrayAdapter<Category> {

    private Context context;
    private List<Category> objects;

    public CategoryAdapter(Context context, int resource, List<Category> objects) {
        super(context, resource, objects);

        this.context = context;
        this.objects = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.row_category, null);
        }

        TextView tvId = convertView.findViewById(R.id.tvId);
        TextView tvName = convertView.findViewById(R.id.tvName);

        tvId.setText(objects.get(position).getId() + "");
        tvName.setText(objects.get(position).getName());

        return convertView;
    }
}
